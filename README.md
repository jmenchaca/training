Repo for FrontEnd Programming training
===========

Doing exercises in Bash and Python to level up.

# Wilkommen to my readme page.
## What's here?

* python
* Front End practice/learning
  * HTML
  * CSS
  * Javascript & JQuery


### Example: Syntax highlighting
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```
