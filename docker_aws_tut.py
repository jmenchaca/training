"""
4/10/2019

AWS Docker tutorial

brew install awscli docker

Create an IAM (Identity and Access Management) administrator user, an admin group, and a key pair associated with a region.

From the AWS menus, select Services > IAM to get started.
To create machines on AWS, you must supply two parameters:
an AWS Access Key ID
an AWS Secret Access Key

Make the VM
docker-machine create --driver amazonec2 --amazonec2-open-port 8000 aws-sandbox

#Print env variables for given machine
docker-machine env aws-sandbox
#Run this to config my local shell, set env variables etc
eval $(docker-machine env aws-sandbox)
# check what machine is active
docker-machine active

#see ip
docker-machine ip aws-sandbox

# logon to th machine
docker-machine ssh aws-sandbox

# Run our actual nginx web server on it
docker run -d -p 8000:80 --name webserver kitematic/hello-world-nginx

-d runs detached and prints id
"""
