# Practice making a GET request
import requests

URL =  "http://maps.googleapis.com/maps/api/geocode/json"

location = "Google headquarters"

PARAMS = {'address':location}

# Assigns response object to variable r
r = requests.get(url = URL, params = PARAMS)

# extracting data in json format
data = r.json()

latitude = data['results'][0]['geometry']['location']['lat']
longitude = data['results'][0]['geometry']['location']['lng']
formatted_address = data['results'][0]['formatted_address']

# print the object PY 3 style
print("Latitude:{}\nLongitude:{}\nFormatted Address:{}".format(latitude, longitude, formatted_address))
