# lab0.2 Python
import sys
# This program is similar to the last program, except that you open a file and process it similarly.
#
# Do the following:
#
# have a variable, e.g. my_file that stores ONLY the path
# For scripting languages (perl, python, ruby, etc.) open the file handle and use that to process the file.
# For a shell environment (bash, ksh, zsh, sh), use a file redirect to direct file to standard input.
# each line my be stored as a variable, e.g. current_line or my_line. Print the variable. Bonus again for line count.
# As this program opens the file, you do not need to use redirect outside of the program, just simply:
#
# ./lab0_2
# ./lab0_2 file_name

# Have one variatble assigned to the actual file
# my_file = "/home/repos"

# Assign file variable to the first argument on cmd line
file = open(sys.argv[1],"r")

# Use a standard for loop to iterate through
for line in file:
    current_line = line
    print(current_line, end='')

# close file after use
file.close()
