
# create class def node
# make a recursive function to print out the Tree
# use an outer loop, with function inside that calls itself

# Define the class we used to navigate the Tree
class Node:
    def __init__(self, root, children):
        self.root = root
        self.children = children
        
# create a tree object of type Node class
tree = Node("Flynn Mackie - Senior VP of Engineering",
            [Node("Wesley Thomas - VP of Design",
                  [Node("Randall Cosmo - Director of Design",
                        [Node("Brenda Plager - Senior Designer", [])])]), Node("Nina Chiswick - VP of Engineering",
                                                                               [Node(
                                                                                   "Tommy Quinn - Director of Engineering",
                                                                                   [Node(
                                                                                       "Jake Farmer - Frontend Manager",
                                                                                       [Node(
                                                                                           "Liam Freeman - Junior Code Monkey",
                                                                                           [])]), Node(
                                                                                       "Sheila Dunbar - Backend Manager",
                                                                                       [Node(
                                                                                           "Peter Young - Senior Code Cowboy",
                                                                                           [])])])])])



# define function that does the printwork
def printtree(node,tab):
    print('\t' * tab + node.root) # first print root node
    tab +=1

    for child in node.children: #recurse thorugh using for loop
        printtree(child, tab)

printtree(tree, 0) # call printtree starting w tab = 0
