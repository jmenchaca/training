"""
cw_ipvalidation

Write an algorithm that will identify valid IPv4 addresses in dot-decimal format. IPs should be considered valid if they consist of four octets, with values between 0 and 255, inclusive.

Input to the function is guaranteed to be a single string.

Examples
Valid inputs:

1.2.3.4
123.45.67.89

POA:
conditions

we know it has to be all digits .isdigit()
    if its all digits, start testing use regex that we know it has four octets,
    expect 3 digits, followed by ., followed by 3 digits etc.

re.match looks for a match *from the beginning* of string
re.search looks for pattern *ANYWHERE* in string

re.match( r'(.*) are (.*?) .*', line, re.M|re.I)
re.I = case insensitive matching
re.M
re.search(pattern, string, flags=0)

re{ n}
match exactly n number of expressions

Identifiers
'(ab)*'
\d any number
\D anything but a num
\w any char
\W anything but a char
. any char excpet newline
\b whitespace around words

Quantifier: amount 
{1,3} : looking for 1-3
+ match 1 or more
? match 0 or more
* match 0 or more
^ match beg of string
$ match end of str
| either or
[] range
{x} expecting x amount


"""
import re

strng = '334.234.314.333'

# def is_valid_IP(strng):
#     #if strng.isdigit():
#         octet = 0
#         iphunter = ''
#         for chunk in range(0, len(strng)):
#             iphunter = re.match(r'\d{1,3}\.', strng, flags=0)
#             octet +=1
#         return iphunter.group()

# matchObj = re.match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.', strng)
matchObj = re.match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', strng)
if matchObj:
    print("matchObj.group() is "  ,matchObj.group())
else:
    print("NO MATCH!!")

"""
phone = "2004-959-559 # This is Phone Number"

# Delete Python-style comments
num = re.sub(r'#.*$', "", phone)
print("Phone Num : ", num)

# Remove anything other than digits
num = re.sub(r'\D', "", phone)
print("Phone Num : ", num)
"""
# print(is_valid_IP('334.234.314.133'))
