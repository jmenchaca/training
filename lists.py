"""lists training!!

Fucntions for lists

1) len() returns how many elements are in a list.
2) max() returns the greatest element of the list. How the greatest element is determined depends on what type objects are in the list. The maximum element in a list of numbers is the largest number. The maximum elements in a list of strings is element that would occur last if the list were sorted alphabetically. This works because the the max function is defined in terms of the greater than comparison operator. The max function is undefined for lists that contain elements from different, incomparable types.
3) min() returns the smallest element in a list. min is the opposite of max, which returns the largest element in a list.
4)
sorted() returns a copy of a list in order from smallest to largest, leaving the list unchanged.
"""

days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday',
'sunday']

# practice grabbing a specific index
print("Lets grab the 3rd, index[2] day of the week: {}".format(days[2]))

print("we have {} items in our list".format(len(days)))
