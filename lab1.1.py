#lab1.1.py
# Using parallel arrays

# Open file
file = open("etcpasswd")

# GetUsers function creates the array of users from the file
# returns a list/array of users. Load array with users
def getUsers():
    for i in range(len(file)):
        users.append(i)   # need to append just the split portion
#        break apart and extract just the user portion of each line
    return users

# returns list of shells
def getShells():

    return shells

# def getShellUsage():  #use a count loop to combine these
#     for i in range(0, users.length())
#

#####################
# print_shell_usage() - given two lists (arrays), print out
#  shell usage 'account: shell'.
#######
def print_shell_usage(users, shells): None
    for i in range(len(users)):
        print("{} {}".format(users[i], shells[i]))

users = getUsers()
shells = getShells()
print_shell_usage(users, shells)

print users #print the users array we built in getUsers function



# Python hash
# dict = {}
# dict['a'] = 'alpha'
# dict['b'] = 'gamma'
# dict['o'] = 'omega'
#
# print dict

# # loop through dict and print keys only
# for key in dict:
#     print keys

#
# def apache_output(line):
#     split_line = line.split()
#     return {'remote_host': split_line[0],
#             'apache_status': split_line[8],
#             'data_transfer': split_line[9],
#     }
