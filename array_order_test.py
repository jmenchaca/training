arr = [10, 3, 50, 2]
#arr = [3, 5, 7, 10]

def in_asc_order(arr):
    i = 0
    if len(arr) == 1: #if only one item in array its true
        bool = True
    else:
        for i in range(len(arr)-1): # Use range(len(arr)-1), so that our arr[i+1] grabs last index
            # lets compare side by side values
            if arr[i] < arr[i+1]:
                bool = True
                i+=1
            else: # when left index value not less than right
                bool = False
                print("Not an ascending array!")
                break
    # if bool is true, its ascending according to my test
    if bool == True:
        print("\n", arr)
        print("We've got a winner! Ascending array it is")
    return bool

# run our function w arr as input
in_asc_order(arr)
