#heaps / binary tree /
"""
Hierarchical data Construct

Top node is called 'root'

numbers linked together

Two types of heaps, min and maxself.
Min heap: where each parent node is <= to child node
Max heap is where each parent node >= its child

Max heap is great for creating a priority queue
where the higher nodes have priority to be processsed
Heaps use py module heapq, its to represent
a priority queue. The property of this struct is that eac

HEAP FUNCTIONS

heapify: converts a list to a heap. In resulting heap, smallest element
         is pushed to index 0, but other elements arent necessarily sorted.

heappush: This fn addas an element to heap without altering curr heap

heappop: fn return smallest data element from heap

heapreplace: fn replaces smallest data element w new value given
"""

import heapq

H = [21,1,45,78,3,5]
# use heapify to rearrange elements
heapq.heapify(H)
print(H)

# Add element to heap @ last index
heapq.heappush(H,8)
print(H)

# Removes element at index 0, first index
heapq.heappop(H)
print(H)

 
