# enumerate
# Practice using enumerate in loops

arr = ['yukiko', 'mari', 'takeshi', 'miko', 'yuri']

# where i (first variable)  is the index value
for i, j in enumerate(arr):
    print('i: ',i)

#create a dictionary using enumerate(arr)
new_dict = dict(enumerate(arr))
print("Here's new dict: ",new_dict)
print("Here's new dict using .format: {}".format(new_dict))

# list comprehension style
[print(i,j) for i,j in enumerate(new_dict)]
