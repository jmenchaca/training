"""
Practice using the with open statement,
pythonic way to open file, and have it closed cleanly

function : called by NAME, you can pass it data
method: piece of code called ON an object. Ethan.makehappylife()

This method is called to act on Ethan, the object

1) Create function to do the regex match
2) create with open stmnt that calls findme function
3) Use hash inside iwth open statmeent to find collect msgs
"""
import re

# make findme function that accepts each line as input to regex through
def findme(line): # whats the input here?
    m = re.match(r"^(.*?\d\d\:\d+)", line).group(1)
    return m
    # re.search allows it to find a match anywhere in string
    # re.match forces to start from beginning. Can use group method

timehash = {}
with open('fake.log') as f:
    for line in f: # for line in file object
        time = findme(line) # time is the result of running the fn on line
        if time not in timehash: # if time not in my hash, initialize to 1
            timehash[time] = 1
        else:  # we've seen you before, incrmeent the counter
            timehash[time] +=1

for time, count in timehash.items(): # use for loop to print
    print(time, count)



# working on some code to detect broken regex, fail nicely..

    # if re.match(r"(\d\d\:\d+)", line).group(1):  # python match function
    #     m = re.match(r"(\d\d\:\d+)", line).group(1)
    #     return m # returns what we found as string
    # else:
    #     #print('m is of type: {}'.format(type(m)))
    #     print('nooooo m is not valid, type: {}').format(type(m))
