# cw_getmidchar
"""
You are going to be given a word.
Your job is to return the middle character of the word.
If the word's length is odd, return the middle character.
If the word's length is even, return the middle 2 characters.

use if condition to catch odd numbers
calculate middle character len(s) / 2
else (its an even string) grab the middle two characters
s[mid] + s[mid + 1]

stars
len(s) / 2

"""
def get_middle(s):
     if len(s) % 2: # odd number true test
         return s[int(len(s)/2)]
     else:
         return s[int(len(s)/2)-1:int(len(s)/2)+1]


s = 'starred'

print(int(len(s)/2)+1)
print('Test grabbing the mid character of odd str: {}'.format(s[int(len(s)/2)]))
print("Hello Mr.Andersen, you're looking for this middle char/s: {}".format(get_middle(s)))
