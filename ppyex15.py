# Practice Python

# Write a program (using functions!) that asks the user for a long
# string containing multiple words. Print back to the user the same string,
# except with the words in backwards order. For example, say I type the string:

#
shortstring = "somestring really long is so long poodeedoo \
:extend me to newline"

print(shortstring)
