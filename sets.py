"""sets.py

- For collecting unique elemnets
-  Use {} curly brackets to make one flavors = {"cherry", "vanilla", "chocolate"}
-  UNordered
- mutable


take a list for example, lets remove the duplicates
also sorts it!


"""

nums = [1, 4, 1, 1, 5, 6, 7, 7, 2, 0]
unique_nums = set(nums)
print(unique_nums)

"""
also can do membership stuff

"""
fruit_salad = {"apple", "banana", "orange", "grapefruit"}

print("apple" in fruit_salad)

# we can also add an element with add() method
fruit_salad.add("peach")
print("Fruits in our salad: {}".format(fruit_salad))

# Popping an element
fruit_salad.pop() # Removes a random element

a = [1, 2, 2, 3, 3, 3, 4, 4, 4, 4]
b = set(a)
print(len(a) - len(b))
# Lenght of a is 6, counts each num in the list.
print("b's value is: {}".format(b))
