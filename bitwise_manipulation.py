"""

Bitwise manipulation

way to manipulate data on very low level

Opersations w bits are used in data compression, encoding, decoding
for more efficiency

Operators

NOT
AND
OR
XOR
"""




# Bitwise Operators
OR
a | b # 0 | 1 = 1
AND
a & b # 0 & 1 = 0

XOR:  If both bits in the compared position of the bit patterns are 0 or 1,
the bit in the resulting bit pattern is 0, otherwise 1.
Difference is 1, sameness 0
a ^ b #
# xor eXclusive or
# 0 ^ 0 = 0
# 0 ^ 1 = 1
# 1 ^ 0 = 1
# 1 ^ 1 = 0

LEFT SHIFT: squares value ^2
a << b
1 << 1 = 2 = 21
1 << 2 = 4 = 22 1 << 3 = 8 = 23
1 << 4 = 16 = 24

RT SHIFT: roots it
a >> b
4 >> 1 = 2
6 >> 1 = 3
5 >> 1 = 2
16 >> 4 = 1

# 4 2 1
a = 5 # 101
b = 3 # 011

c = a | b # 7, 101 | 011 = 111 = 7
d = a & b # 1, 101 & 011 = 001 = 1
e = a ^ b # 6, 101 ^ 011 = 110 = 6

BITWISE explanation link
https://www.hackerearth.com/practice/basic-programming/bit-manipulation/basics-of-bit-manipulation/tutorial/
############# XOR #############
# https://en.wikipedia.org/wiki/Exclusive_or############# XOR #############

############# SHIFTING #############
# 4 2 1
a = 5 # 101
b = 3 # 011

f = a << b # 40
e = a >> b # 0
############# EXAMPLES #############




https://www.tutorialspoint.com/python/bitwise_operators_example.htm
