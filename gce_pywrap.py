   # GCE py wrapper to convert json to dict
# then feed it into py
# pull in our json library
import json

#json_string = pyopen("gce.json")

# THis loads the json into a dict/hash
# We want to pull in whole file, read it, then pass
# it to gedict to process the json string
gcedict = json.loads(open("gce.json").read())

#for loop to Iterate through the dict items

print(gcedict['dk-prod-tools'])
# print(gcedict)

# use heredoc, triple quotes f""" {a} {b} {c}
# Can create machines in gcloud using gce command line w/ options
# and rest -> post command

#  gcloud comepute instances create
