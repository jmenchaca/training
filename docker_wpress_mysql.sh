#######
#!/usr/bin/env bash

# Wordpress + MySQL project

# Make the volume
# Create & run mysql db
#create * run wpress container

#Create base structure for both the containers
# https://docs.docker.com/storage/volumes/


#docker run --name mysqldb -e MYSQL_ROOT_PASSWORD=boringdefault -d mysql:tag \
#-v dbvolume


docker run --name wpress --network some-network -d wordpress -p 8000:80 -v wpvolume:/ \
# The following environment variables are also honored for configuring your WordPress instance:
-e WORDPRESS_DB_HOST=wpress \
-e WORDPRESS_DB_USER=admin \
-e WORDPRESS_DB_PASSWORD=boringdefault \
-e WORDPRESS_DB_NAME=db \
/
#-e WORDPRESS_TABLE_PREFIX=...



########
