# Python lists
"""
len() : returns how many elements in a lists
max() : returns greatest element of list
min() : returns smallest element
sorted(): returns copy of list from smallest to largest

join() : takes list of strings as argument, returns string separated by separator
"""
# Create a list of strings sepearated by separator
new_str = ": ".join(["fore", "aft", "starboard", "port"])
print(new_str)

names = ["carol", "albert", "ben", "donna"]
print(" & ".join(sorted(names)))
