# ex14.py
from sys import argv

script, user_name = argv
prompt = '::: '

print(f"Hi there {user_name}, I'm the {script} script.")
print("I'd like to ask you a few questions.")
print(f"Do you like this script?")
likes = input(prompt)

print(f"Where do you live {user_name}?")
lives = input(prompt)

print(f"What type of languages do you like?")
language = input(prompt)

# print results using a multilne string
print(f"""
Alright, so you said {likes} about liking this script.
You live in {lives}. I dont know that place!
You like the language {language}.
""")
