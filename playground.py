
"""
1) create result list
2) use for loop to go through
"""
s = "asdf09823895023895adsf"

def solution(s):
    # use range to aid split
    results = []
    if len(s) % 2:
        s += '_'

    for chunk in range(0, len(s), 2):
        print("chunk: {}".format(chunk))
        results.append(s[chunk:chunk+2])
        print(s[chunk:chunk+2])

    return results

print(solution(s))

print('6 % 2 is {}'.format(6 % 2))
print(5 % 2)
print(4 % 2)
print(3 % 2)
