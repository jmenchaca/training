# Summing numbers digits from inputted number
# arr = [4, 5, 6, 7]
#
# def sumDigits(number):
#     return sum([int(i) for i in str(abs(number))])
#
# for i in range(len(arr))
#
#
# n = 123456789
# print(sumDigits(n))



"""
make all the digits absolute
use for loop to go thoruhg lsit and add up individual nums
return sum

range of lenght of a number
"""

def sumDigits(number):
    sum = 0
    for num in str(abs(number)):
        sum += int(number)
    return sum

number = 123456789
print(sumDigits(number))
