# Python Dictionaries

#Printing out key and value
# Items returns list of dicts (key, value) tuple pairs
cast = {
           "Jerry Seinfeld": "Jerry Seinfeld",
           "Julia Louis-Dreyfus": "Elaine Benes",
           "Jason Alexander": "George Costanza",
           "Michael Richards": "Cosmo Kramer"
       }

for key, value in cast.items():
    print("Actor: {}      Role {}".format(key,value))

# a key value system,
# Keys are in double quotes, followed by : , then value
elements = {"hydrogen": 1, "helium": 2, "carbon": 6}

zoo_animals = {"elephants": 3, "kangaroos": 4, "tortoise": 2, "hippo": 4, "baboon": 1}

for key, value in zoo_animals.items():
    print("animal: {}      quantity: {}".format(key, value))

# sort the zoo zoo_animals
zoo_sort = sorted(zoo_animals)
print("Here's our sorted animal list: {}".format(zoo_sort))

# Lookup / pull value of a key by by using []
# We call the dict elements['carbon'] and put in sq brackets the value
# we want to get out
# print("Let's print an item thats not in the dict: elements[hefel]".format(elements['hefel']))

# Add a new key or change a keys value
elements['Lithium'] = 3

# Check if a key is in a Dict
print('magicshmiz' in elements)

# Using the get() method on a dict
# returns none in case key isnt found
# Won't KILL program if cant find key
print("Do we have the key 'hefel' in our dict?: {}".format(elements.get('hefel', 'No such key in here!')))

# Identity operators: used to check if key is in dict or not
# is evaulates if both sides have same Identity
# is not evaulates if both sides have different identities

# With identity operator IS, it determines if the

print ("\nIdentity operators")
n = elements.get("dilithium")
print(n is None)
print(n is not None)

a = [1, 2, 3]
b = a
c = [1, 2, 3]
print(a == b)
print(a is b)
print(a == c)
print(a is c)

########################################################
# Compount Dicts
########################################################

elements = {"hydrogen": {"number": 1,\
                        "weight": 1.00794,\
                        "symbol": "He"}\
            }

helium = elements["helium"]
hydrogen_weight = elements["hydrogen"]["weight"]  # hydrogens weight

# Add elemeent to the dict
# hydro

elements = {"hydrogen": {"number"}}
#Add an element/item in the Dict
oxygen = {"number":8, "weight":15.999, "symbol":"O"}
print(elements)
