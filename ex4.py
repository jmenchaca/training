# LPTHW ex 4: Variables and names

# Assign cars value of 100
cars = 100
# Assign space_in_a_car float value 4.0
space_in_a_car = 4
# Assign drivers to 30
drivers = 30
# Assign passengers value of 90
passengers = 90
# Assign cars_not_driven value of cars - drivers
cars_not_driven = cars - drivers
# Assigns num drivers to cars_driven
cars_driven = drivers
# Assigns product
carpool_capacity = cars_driven * space_in_a_car
average_passengers_per_car = passengers / cars_driven

coolthing = "cool"

print("There are", cars, "cars available.") #Smudge in the cars variable inline
print("There are only", drivers, "drivers available.")
print("There will be", cars_not_driven, "empty cars today.")
print("We can transport", carpool_capacity, "people today.")
print("We have", passengers, "to carpool today.")
print("We need to put about", average_passengers_per_car, "in_each_car.")

# do a printline 
print("Let's do a little practice embedding a string in", placeholder, "in here")
