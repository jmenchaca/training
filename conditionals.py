# Udacity conditionals
# BMI range from 1.5 - 24.9 considered healthy

weight = 86
height = 192 # in cm
bmi = (weight / height**2)*10000 # get out of cm
print("Your BMI is: {}".format((bmi)))

if 18.5 <= bmi < 25:
    print("BMI is considered 'normal'")
elif bmi > 25:
    print("Brrrrr, you a hefel! Get on the treadmill")
