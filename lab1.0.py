# lab1 in python

# Instructions: Do something like the following:
# loop through line by line of the passwd file.
# extract user account and shell fields
# print the user account and shell fields
# as follows User: /usr/bin/shell

# open the file
file = open("etcpasswd")

for line in file:
    #Grab user off the front of string using delimter
    #create some lists to store the users and shells
    # Whatever is after delimiter is shell
    # str.split(str) split a string, using 'str' as delimiter
    user, shell = line.split(':')
    # print "%-10s:%s" % (user,shell) # Defunct Py2 style formatting
    # Python 3 method of str formatting using {} {} {}{}
    print("{:10} {}".format(user, shell),end='')

# Python 3 pads from left, i guess more logical? meh
string = "Tom"
print("{} {:^3} {}".format("Tom", "n", "Jerry")) # :^3 means use 3 spaces of padding but center the item
print("Default padding ex:{:10}".format(string))
print("Left alignment padding:{:_<10}".format(string)) # DEFAULT, left alignment
print("Right alignment padding:{:_>10}".format(string)) # right alignment


    # some purdy formatting methods. The number after index: fieldsize/num spaces
    # print("Sammy has {0:4} red {1:16}!".format(5, "balloons"))
file.close()
